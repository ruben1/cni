CNI 2.0e
========

Requeriments
------------
*   PHP Version 5.2
*   PDO support enabled
*   PDO drivers MySQL

Libraries
---------
*   Prototype (all sections except entradas2)
*   jQuery 1.4.4 ( entradas2 )
*   jQuery UI 1.8.8 ( entradas 2 )
*   jpgraph ( entradas 2)
*   ezpdf
*   Zend Framework 1.x ( rapido )
*   PEAR
*   Twitter Bootstrap 2.0.4 ( servicont )
